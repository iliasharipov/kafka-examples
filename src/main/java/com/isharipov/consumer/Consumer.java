package com.isharipov.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Arrays;
import java.util.Properties;

public class Consumer {
    public static void main(String[] args) {
        Properties properties = new Properties();
        // https://kafka.apache.org/documentation/#newconsumerconfigs
        properties.setProperty("bootstrap.servers", "127.0.0.1:9092");
        String deserializer = StringDeserializer.class.getName();
        properties.setProperty("key.deserializer", deserializer);
        properties.setProperty("value.deserializer", deserializer);

        properties.setProperty("group.id", "test");
        properties.setProperty("enable.auto.commit", "true");
        properties.setProperty("auto.commit.interval.ms", "1000");
        properties.setProperty("auto.offset.reset", "earliest");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
        consumer.subscribe(Arrays.asList("first_topic", "second_topic"));
        StringBuilder sb = new StringBuilder();
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {
                sb.append("Partition: ").append(record.partition())
                        .append(", Offset: ").append(record.offset())
                        .append(", Key: ").append(record.key())
                        .append(", Value: ").append(record.value());
                System.out.println(sb.toString());
                sb.setLength(0);
            }
        }
    }
}