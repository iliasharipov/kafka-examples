package com.isharipov.producer;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class KafkaProducer {
    public static void main(String[] args) {
        Properties properties = new Properties();
        // https://kafka.apache.org/documentation/#producerconfigs
        properties.setProperty("bootstrap.servers", "127.0.0.1:9092");
        String serializer = StringSerializer.class.getName();
        properties.setProperty("key.serializer", serializer);
        properties.setProperty("value.serializer", serializer);
        //producer acks
        properties.setProperty("acks", "1");
        properties.setProperty("retries", "3");
        properties.setProperty("linger.ms", "1");

        Producer<String, String> producer = new org.apache.kafka.clients.producer.KafkaProducer<String, String>(properties);
        for (int i = 0; i < 100; i++) {
            ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>("first_topic", "message test");
            producer.send(producerRecord);
        }
        producer.close();
    }
}